<div id="cb"></div>

# Windows
> From source code to a built and fully working agent

## Changelog

- [Version & Git Revision](_agent-docu.md?id=cb-co)
- [Config.ini](_agent-docu.md?id=cb-i)
- [Change TrayIcon Menu](_agent-docu.md?id=cb-i-iwp)


 >Note: All changelogs will be done in the src directory of the project.<br>
Example: arduino-create-agent/src/github.com/arduino/arduino-create-agent


<div id="cb-co"></div>

* ### Version & Git Revision
In the **main.go** file, set the variable <code>version</code> on line 28 to your agent version. Ex: **<code>version = "2.1.0"</code>** <br> Aswell as the variable <code>git_revision</code> on line 29 to the git revision, in this case to null. Ex: **<code>git_revision = ""</code>**

```       
28 -	version               = "2.1.0" 
29 -	git_revision          = "" 
```

<div id="cb-i"></div>

* ### Config.ini
Add your website's url to the <code>origins</code> variable. <br>
Ex: **<code>origins = http://webide.arduino.cc:8080, https://cherpa.io</code>**

```
10 -	origins = http://webide.arduino.cc:8080, https://cherpa.io

```


<div id="cb-i-iwp"></div>


* ### Change TrayIcon Menu
To change the icon in the traybar, navigate to the **trayicon.go** file. <br>
Proceed by changing the desired items in the <code>setupSysTrayReal</code> function on line **64**. <br>
Changable Strings:

```
67 - 	mUrl := systray.AddMenuItem("Open Cherpa Projects", "Cherpa")
68 -	mDebug := systray.AddMenuItem("Open debug console", "Debug console")
69 - 	menuVer := systray.AddMenuItem("Agent version "+version+""+git_revision,"")
70 - 	mPause := systray.AddMenuItem("Pause Plugin", "")
71 - 	//mQuit := systray.AddMenuItem("Quit Plugin", "")

```


<div id="cb-v"></div>

<!-- - [Generate Certificates](_agent-docu.md?id=cb-i-di) -->

<div id="cb-i-di"></div>

## Generate Certificate
> This is done in the src folder of the project.

To generate certificates, navigate to the **main.go** file and change the flag from <code>false</code> to <code>true</code> on line **41**. <br>
Ex: **<code>genCert = flag.Bool("generateCert", true, "")</code>**

```
From:    41 -  genCert = flag.Bool("generateCert", false, "")
To:	  41 -  genCert = flag.Bool("generateCert", true, "")
```


<div id="cb-v-vp"></div>


## Changing the Agent's Icon.
> This is done in the src folder of the project.

- Step 1 - Get an **ICO** image of your desired icon with a transparent background. <br>
- Step 2 - Open CMD and navigate to the **icon** folder in the project.<br>
	- **Directory:** <code>arduino-create-agent/src/github.com/arduino/arduino-create-agent/icon</code>
- Step 3 - Copy your **logo.ico** icon to the directory.

- Step 4 - Run the following command in CMD: **<code>make_icon.bat logo.ico</code>**

- Step 5 - The following file will be generated: **iconwin.go**. Now you're good to go! Compile so that the changes take place.


<div id="cb-v-vv"></div>

## Building ✔️
Here is how to build the source code.
> Keyword: **root directory > arduino-create-agent file**

- Step 1 - Download & Open **Git Bash** or **Cygwin** and navigate the the root directory.
- Step 2 - Run the following command: **<code>export GOPATH=$PWD</code>**
- Step 3 - Navigate to the **src** directory using **Git Bash** or **Cygwin**: <code>arduino-create-agent/src/github.com/arduino/arduino-create-agent/</code>
- Step 4 - Run the following command: **<code>go build -ldflags "-H=windowsgui" </code>**

<p class="tip">Note: you can add a -v after go build command to debug the build process </p>


<div id="cb"></div>

<hr>

# Mac
> From source code to a built and fully working agent

## Changelog

- [Version & Git Revision](_agent-docu.md?id=cb-co)
- [Config.ini](_agent-docu.md?id=cb-i)
- [Change TrayIcon Menu](_agent-docu.md?id=cb-i-iwp)


<p class="tip">Note: All changelogs will be done in the src directory of the project.<br>
Example: arduino-create-agent/src/github.com/arduino/arduino-create-agent/</p>


<div id="cb-co"></div>

* ### Version & Git Revision
In the **main.go** file, set the variable <code>version</code> on line 28 to your agent version. Ex: **<code>version = "2.1.0"</code>** <br> Aswell as the variable <code>git_revision</code> on line 29 to the git revision, in this case to null. Ex: **<code>git_revision = ""</code>**

```       
28 -	version               = "2.1.0" 
29 -	git_revision          = "" 
```

<div id="cb-i"></div>

* ### Config.ini
Add your website's url to the <code>origins</code> variable. <br>
Ex: **<code>origins = http://webide.arduino.cc:8080, https://cherpa.io</code>**

```
10 -	origins = http://webide.arduino.cc:8080, https://cherpa.io

```


<div id="cb-i-iwp"></div>


* ### Change TrayIcon Menu
To change the icon in the traybar, navigate to the **trayicon.go** file. <br>
Proceed by changing the desired items in the <code>setupSysTrayReal</code> function on line **64**. <br>
Changable Strings:

```
67 - 	mUrl := systray.AddMenuItem("Open Cherpa Projects", "Cherpa")
68 -	mDebug := systray.AddMenuItem("Open debug console", "Debug console")
69 - 	menuVer := systray.AddMenuItem("Agent version "+version+""+git_revision,"")
70 - 	mPause := systray.AddMenuItem("Pause Plugin", "")
71 - 	//mQuit := systray.AddMenuItem("Quit Plugin", "")

```


<div id="cb-v"></div>

<!-- - [Generate Certificates](_agent-docu.md?id=cb-i-di) -->

<div id="cb-i-di"></div>

## Generate Certificate
> This is done in the src folder of the project.

To generate certificates, navigate to the **main.go** file and change the flag from <code>false</code> to <code>true</code> on line **41**. <br>
Ex: **<code>genCert = flag.Bool("generateCert", true, "")</code>**

```
From:    41 -  genCert = flag.Bool("generateCert", false, "")
To:	  41 -  genCert = flag.Bool("generateCert", true, "")
```


<div id="cb-v-vp"></div>


## Changing the Agent's Icon.
> This is done in the src folder of the project.

- Step 1 - You will need 3 **ICO** images of your desired icon with a transparent background. <br>
	- Light Icon
	- Dark Icon
	- Hibernate Icon

- Step 2 - Open the **terminal** and navigate to the **icon** folder in the project.<br>
	- **Directory:** <code>arduino-create-agent/src/github.com/arduino/arduino-create-agent/icon</code>
- Step 3 - Copy your icons to the icon to the directory.

- Step 4 - Run the following command in the terminal: **<code>make_icon.sh (LightIconName).ico </code>**

- Step 5 - It will generate a file called icondarwin.go 

- Step 6 - Make a temporary directory and copy it there.

- Step 7 - Then do the same for the dark icon. After that backup the file aswell.

- Step 8 - Do the same for (HibernateIcon).ico, and back it up.

- Step 9 - Open all 3 files in sublime, and create a new file with the below content.

```
//+build darwin

// File generated by 2goarray v0.1.0 (http://github.com/cratonica/2goarray)

package icon

import (
	"os/exec"
	"strings"
)

func GetIcon() []byte {
	cmd := exec.Command("defaults", "read", "-g", "AppleInterfaceStyle")
	output, _ := cmd.Output()
	if !strings.Contains(string(output), "Dark") {
		return Data
	} else {
		return DataLight
	}
}

func GetIconHiber() []byte {
	return DataHibernate
}

var Data []byte = []byte{

}

var DataHibernate []byte = []byte{

}

var DataLight []byte = []byte{

}
```

- Step 10 - Copy each backuped file's corresponding array content to the new file arrays.

- Step 11 - Save the new file as **icondarwin.go** in the icon folder.

- Step 12 - The following file will be generated: **iconwin.go**. Now you're good to go! Compile so that the changes take place.


## Building ✔️
Here is how to build the source code.
> Keyword: **root directory > arduino-create-agent file**

- Step 1 - Run the **terminal** and navigate the the root directory.
- Step 2 - Run the following command: **<code>export GOPATH=$PWD</code>**
- Step 3 - Navigate to the **src** directory using the **terminal**: <code>arduino-create-agent/src/github.com/arduino/arduino-create-agent/</code>
- Step 4 - Run the following command: **<code>go build" </code>**

<p class="tip">Note: you can add a -v after go build command to debug the build process </p>

<div id="signing"></div>

## Signing the agent.
To sign the agent navigate to the agent's directory and run the following commands in the terminal.

- Step 1 - Get the developer hash-identitfier by running **<code>security find-identity -v</code>**
- Step 2 - Then run **<code>codesign -s hash-identitfier agent-name</code>**

<div id="packaging"></div>

## Packaging the agent.
To package the agent:
- Step 1 - Open a text editor and paste the following code.
	- https://pastebin.com/raw/iDuiQUyq

- Step 2 - Save it in the agent's directory as **appify.sh**.
- Step 3 - Open teh teminal and navigate to the directory needed.
- Step 4 - Run the following command **<code>bash appify.sh agent-name</code>**



<div id="cb-v-vv"></div>

